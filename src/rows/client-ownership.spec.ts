import { isAbortError } from "abort-tools";
import assert from "assert";
import * as pg from "pg";
import { queryDelete, queryInsert, withTransaction } from "table-access";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";
import { clientOwnershipRowDescriptor, createClientOwnershipRowEvents } from "./client-ownership.js";
import { clientRowDescriptor } from "./client.js";
import { userRowDescriptor } from "./user.js";

test("client-ownership", t => withContext(async context => {
    await withTransaction(context.pool, initializeMocks);

    const abortController = new AbortController();
    const eventIterable = createClientOwnershipRowEvents(
        context.pool,
        {},
        error => t.ok(isAbortError(error), "expected AbortError"),
        abortController.signal,
    );
    const eventIterator = eventIterable[Symbol.asyncIterator]();

    try {
        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type == "snapshot");
            t.deepEqual(
                eventNext.value.rows,
                [
                ],
            );
        }

        await withTransaction(context.pool, async clientOwnership => {
            await queryInsert(clientOwnership,
                clientOwnershipRowDescriptor,
                {
                    user_id: "\\xaa",
                    client_id: "\\xcc",
                    created_utc: "2022-04-12T11:31:20",
                },
            );
        });

        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type == "insert");
            t.deepEqual(
                eventNext.value.key,
                {
                    user_id: "\\xaa",
                    client_id: "\\xcc",
                },
            );
            t.deepEqual(
                eventNext.value.data,
                {
                    created_utc: "2022-04-12T11:31:20",
                },
            );
        }

        await withTransaction(context.pool, async clientOwnership => {
            await queryDelete(clientOwnership,
                clientOwnershipRowDescriptor,
                {
                    user_id: "\\xaa",
                    client_id: "\\xcc",
                },
            );
        });

        {
            const eventNext = await eventIterator.next();
            assert(eventNext.done !== true);
            assert(eventNext.value.type == "delete");
            t.deepEqual(
                eventNext.value.key,
                {
                    user_id: "\\xaa",
                    client_id: "\\xcc",
                },
            );
        }
    }
    catch (error) {
        if (!abortController.signal.aborted) throw error;
    }
    finally {
        abortController.abort();
        await eventIterator.next().catch(error => t.ok(isAbortError(error), "expected AbortError"),
        );
    }
}));

async function initializeMocks(client: pg.ClientBase) {
    await queryInsert(client, userRowDescriptor, {
        id: "\\xaa",
        name: "testy",
        created_utc: "2022-04-12T11:30:35",
        salt: "\\xAB",
    });

    await queryInsert(client, clientRowDescriptor, {
        id: "\\xcc",
        name: "test-client",
        created_utc: "2022-04-12T11:30:35",
        salt: "\\xCD",
    });
}
