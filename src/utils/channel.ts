import { assertAbortSignal } from "abort-tools";
import { createBufferedIterable } from "buffered-iterable";
import * as pg from "pg";

export async function listenChannel(
    client: pg.ClientBase,
    channel: string,
    keepaliveInterval: number,
    signal: AbortSignal,
): Promise<AsyncIterable<string>> {
    const iterable = createBufferedIterable<string>();
    signal.addEventListener("abort", () => iterable.done());

    const onInterval = async () => {
        await client.query("");
    };

    const onNotification = (notification: pg.Notification) => {
        if (notification.channel !== channel) return;

        clearInterval(keepaliveTimer);

        keepaliveTimer = setInterval(
            () => onInterval().catch(onError),
            keepaliveInterval,
        );

        iterable.push(notification.payload ?? "");
    };
    const onError = (error: unknown) => {
        iterable.error(error);
    };

    let keepaliveTimer = setInterval(
        () => onInterval().catch(onError),
        keepaliveInterval,
    );

    client.addListener("error", onError);
    client.addListener("notification", onNotification);

    await client.query(`LISTEN ${client.escapeIdentifier(channel)}`);

    return {
        async *[Symbol.asyncIterator]() {
            try {
                yield* iterable;
            }
            finally {
                clearInterval(keepaliveTimer);

                client.removeListener("error", onError);
                client.removeListener("notification", onNotification);

                await client.query(`UNLISTEN ${client.escapeIdentifier(channel)}`);
            }

            assertAbortSignal(signal, "channel aborted");
        },
    };

}
