import { isAbortError } from "abort-tools";
import assert from "assert";
import { second } from "msecs";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";
import { parseBigNotifications } from "./big-notification.js";
import { listenChannel } from "./channel.js";

test("big-notification", t => withContext(async context => {
    const channel = "ch-123";
    const message = "0123456789";

    const client = await context.pool.connect();
    try {
        const abortController = new AbortController();
        const notifications = await listenChannel(
            client, channel, 10 * second, abortController.signal,
        );
        const bigNotifications = parseBigNotifications(
            notifications,
        );

        const iterator = bigNotifications[Symbol.asyncIterator]();
        try {
            {
                const chunkSize = message.length;

                await context.pool.query(`
SELECT public.dispatch_big_notification($1, $2, $3);
`, [channel, message, chunkSize]);

                const next = await iterator.next();
                assert(next.done !== true);

                t.equal(next.value.payload, message);
            }

            {
                const chunkSize = message.length - 1;

                await context.pool.query(`
SELECT public.dispatch_big_notification($1, $2, $3);
`, [channel, message, chunkSize]);

                const next = await iterator.next();
                assert(next.done !== true);

                t.equal(next.value.payload, message);
            }

            {
                const chunkSize = 4;

                await context.pool.query(`
SELECT public.dispatch_big_notification($1, $2, $3);
`, [channel, message, chunkSize]);

                const next = await iterator.next();
                assert(next.done !== true);

                t.equal(next.value.payload, message);
            }
        }
        finally {
            abortController.abort();

            await iterator.next().catch(error => t.ok(isAbortError(error), "expected AbortError"));
        }

        client.release();
    }
    catch (error) {
        client.release(true);
        throw error;
    }

}));
