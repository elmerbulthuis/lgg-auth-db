import test from "tape-promise/tape.js";
import { isAfterOffsetCycle } from "./cycle.js";

test("cycle", async t => {
    t.ok(isAfterOffsetCycle(10, 11, 5));
    t.ok(isAfterOffsetCycle(10, 1, 5));
    t.notOk(isAfterOffsetCycle(10, 5, 5));
    t.notOk(isAfterOffsetCycle(10, 10, 5));
});
