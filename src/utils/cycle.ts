export function isAfterOffsetCycle(
    offset: number,
    value: number,
    treshold: number,
) {
    if (value > offset) return true;
    if (offset - value > treshold) return true;
    return false;
}
