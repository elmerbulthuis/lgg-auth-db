--@require client-ownership

drop trigger client_ownership_row_event_trg_all
on public.client_ownership;

drop function public.client_ownership_row_event;


perform public.create_row_event_trigger(
    'public',
    'client_ownership_row_event',
    'row-event',
    array['user_id', 'client_id'],
    array['created_utc']
);

create trigger client_ownership_row_event_trg_all after
insert or update or delete
on public.client_ownership
for each row
execute procedure public.client_ownership_row_event()
;
