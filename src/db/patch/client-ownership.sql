--@require initial
--@require row-event

create table public.client_ownership(
    user_id bytea not NULL,
    client_id bytea not NULL,
    created_utc timestamp without time zone NOT NULL
);

alter table public.client_ownership
add CONSTRAINT client_ownership_pk PRIMARY KEY (user_id, client_id),
add constraint client_ownership_user_fk FOREIGN KEY (user_id)
    REFERENCES public.user(id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
add constraint client_ownership_client_fk FOREIGN KEY (client_id)
    REFERENCES public.client(id)
    ON UPDATE CASCADE
    ON DELETE CASCADE;


perform public.create_row_event_trigger(
    'public',
    'client_ownership_row_event',
    'row-event',
    array['user', 'client'],
    array['created_utc']
);

create trigger client_ownership_row_event_trg_all after
insert or update or delete
on public.client_ownership
for each row
execute procedure public.client_ownership_row_event()
;
